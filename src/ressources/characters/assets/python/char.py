#!/usr/bin/python3

number = int(input("De quel nombre voulez vous connaître le caractère utf-8 correspondant ?"))
print(f"Le caractère utf-8 correspondant est {chr(number)}")