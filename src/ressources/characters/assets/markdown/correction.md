## Introduction

###  Écriture en binaire

3 s'écrit 11 en binaire
15 s'écrit 1111 en binaire
17 s'écrit 10001 en binaire
63 s'écrit 111111 en binaire
64 s'écrit 1000000 en binaire
255 s'écrit 11111111 en binaire

## ASCII et UTF-8

### Traduction du prénom en ASCII 
Laurent donnera les nombres suivants dans la table ASCII :
76 97 117 114 101 110 et 116
En binaire, cela fera
01001100 0110000 101110101 01110010 01100101 01101110 01110100

*remarque : le code ASCII ne dépassant jamais 127, tous les octets commencent par 0*

### Utilisation de ghex

Fichier Python ouvert avec ghex. On voit par exemple le 91 (5B en hexadécimal ) pour le crochet ouvrant
![fichier ghex](ressources/characters/assets/img/ghex_python.png)

Lorsque l'on ouvre l'image de fond d'écran, on voit que l'image a été manipulé par Adobe Photoshop sans aucun doute, et le logiciel qui a produit l'image finale est Affinity Photo (encadré rouge du Software Agent) 
![fichier ghex](ressources/characters/assets/img/ghex_background.png)

### Ascii étendu

Pour pouvoir utiliser les accents et les symboles spécifiques à une langue, on utilise des extension de l'ASCII, avec l'utilisation des nombres de 128 à 255 dans l'octet

Au premier temps de l'informatique, le dernier bit était utilisé comme bit de contrôle. La fiabilité grandissante 
du matériel et des protocoles a permis de l'utiliser pou donner un contenu

### ISO-8859-1

Comme son nom l'indique, ISO-8859-1 est une norme ISO, qui est l'organisme mondial de normalisation (International Standardization Organization )

ISO-8859-1 est un codage étendu souvent appelé latin-1. C'est une extension du code ASCII qui permet d'utiliser les numéros entre 128 et 255 pour avoir les différents accents et symbôles qui correspondent aux usages de l'europe occidentale. Cette norme contient 188 caractères imprimables et des caractères de contrôle. 

![iso-8859-1](ressources/characters/assets/img/iso-8859-1.png)

### cp-1252

CP-1252 ou Windows-1252 est une page de code utilisée historiquement par défaut sur le système d'exploitation Microsoft Windows en anglais et dans les pays d'Europe de l'Ouest. C'est une extension de l'ISO-8859-1 propre à Microsoft qui rajoute des caractères imprimables entre 128 et 159 à la place des caractères de contrôle de la norme ISO

Dans l'image ci dessous, les caractères en jaune représente la différence avec la nome ISO et les caractères en vert sont inutilisés

![cp-1252](ressources/characters/assets/img/cp-1252.png)

## UTF-8

### Des accents

Si l'on met dans un fichier "prénom" et qu'e l'on ouvre le fichier avec ghex, à la place du é, on voit deux points qui correspondent à deux octets.

Le premier octet (C3) commence par 110, et le second octet (A9) commence par 10. Cela correspond bien au codage UTF-8 sur 2 bits

![prenom](ressources/characters/assets/img/prenom.png)

### le symbôle euro

En utilisant le site, on voit que le symbôle de l'euro est codé sur 3 octets, et le premier commence par 1110, le second par 10 et le troisième par 10, comme il se doit dans la norme utf-8

![euro](ressources/characters/assets/img/euro.png)