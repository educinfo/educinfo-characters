<?php
/*****************
1ère NSI
entiers relatifs 
******************/
global $pages;

// Bases du javascript
$menu_activite = array(
    "titre" => "Représentation des caractères",
    "contenu" => [
        'introduction',
        'ascii',
        'pythonascii'
    ]
);

// pages des bases du javascript
$characters_pages = array(
    'introduction' => array(
        "template" => 'characters/introduction.twig.html',
        "menu" => 'characters',
        'page_precedente' => NULL,
        'page_suivante' => 'ascii',
        'titre' => 'Introduction',
        "css" => 'ressources/characters/assets/css/characters.css'
    ),
    'ascii' => array(
        "template" => 'characters/ascii.twig.html',
        "menu" => 'characters',
        'page_precedente' => 'introduction',
        'page_suivante' => 'pythonascii',
        'titre' => 'Le code ASCII et utf-8',
        "css" => 'ressources/characters/assets/css/characters.css'
    ),
    'pythonascii' => array(
        "template" => 'characters/pythonascii.twig.html',
        "menu" => 'characters',
        'page_precedente' => 'ascii',
        'page_suivante' => NULL,
        'titre' => 'Manipulation du codage en python',
        "css" => 'ressources/characters/assets/css/characters.css'
    ),
    'CC1987yt67' => array(
        "template" => 'characters/correction.twig.html',
        "menu" => 'characters',
        'page_precedente' => NULL,
        'page_suivante' => NULL,
        'titre' => 'Le codage des caractères - correction',
        "css" => 'ressources/characters/assets/css/characters.css'
    ),
);

$pages = array_merge($pages, $characters_pages);
