<?php

register_activity(
    'characters',
    array(
        'category' => 'NSI1',
        'section' => 'NSI1typebase',
        'type' => 'url',
        'titre' => "Représentation des caractères",
        'auteur' => "Laurent COOPER",
        'URL' => 'index.php?activite=characters&page=introduction',
        'commentaire' => "Un ordinateur, c'est une machine électromécanique qui permet de manipuler des 0 et des 1. Mais alors comment fait on pour manipuler du texte ?",
        'directory' => 'characters',
        'icon' => 'fas fa-cogs',
        'prerequis' => NULL
    )
);