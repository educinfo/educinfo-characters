# Changelog
*educinfo-characters*

Package pour l'apprentissage du codage des caractères (1ère)

Tous les changements notables seront documentés dans ce fichier

Le format s'appuie sur [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
et ce projet adhère au [versionnement sémantique](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0]

### Ajout
- ce changelog
- structure des répertoires
- page.php avec le sommaire
- ajout des pages introduction
- ajout de la page sur le code ascii et utf-8
- ajout de la page sur ord et chr en python
- lien vers la vidéo youtube sur le codage
- différentes images